package ictgradschool.industry.lab15.ex01;

import java.util.ArrayList;
import java.util.List;

public class NestingShape extends Shape {

    private List<Shape> shapes = new ArrayList<>();


    /**
     * Creates a NestingShape object with default values for state.
     */
    public NestingShape() {
        super();
    }

    /**
     * Creates a NestingShape object with specified location values, and default values for
     * other state items.
     *
     * @param x
     * @param y
     */
    public NestingShape(int x, int y) {
        super(x, y);
    }


    /**
     * Creates a NestingShape with specified values for location, velocity and direction.
     * Non-specified state items take on default values.
     *
     * @param x
     * @param y
     * @param deltaX
     * @param deltaY
     */
    public NestingShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }

    /**
     * Creates a NestingShape with specified values for location, velocity, direction, width
     * and height.
     *
     * @param x
     * @param y
     * @param deltaX
     * @param deltaY
     * @param width
     * @param height
     */
    public NestingShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }

    /**
     * Moves a NestingShape object (including its children) with the bounds specified by
     * arguments width and height. Remember that the NestingShape ’s children only want
     * to move within the bounds of the NestingShape itself, rather than the whole screen.
     *
     * @param width  width of two-dimensional world.
     * @param height height of two-dimensional world.
     */
    public void move(int width, int height) {
        super.move(width, height);
        for (Shape shape : shapes) {
            shape.move(width, height);
        }
    }


    /**
     * Attempts to add a Shape to a NestingShape object. If successful, a two-way link is
     * established between the NestingShape and the newly added Shape. This method
     * throws an IllegalArgumentException if an attempt is made to add a Shape to a
     * NestingShape instance where the Shape argument is already a child within a
     * NestingShape instance. An IllegalArgumentException is also thrown when an
     * attempt is made to add a Shape that will not fit within the bounds of the proposed
     * NestingShape object.
     *
     * @param child
     * @throws IllegalArgumentException
     */
    public void add(Shape child) throws IllegalArgumentException {
        if (this.contains(child) || child.parent != null
                || this.fX+this.fWidth < child.fX+child.fWidth || this.fY+this.fHeight < child.fY+child.fHeight) {
            throw new IllegalArgumentException();
        } else {
            this.shapes.add(child);
            child.parent = this;
        }
    }

    /**
     * Removes a particular Shape from a NestingShape instance. Once removed, the
     * two-way link between the NestingShape and its former child is destroyed. This
     * method has no effect if the Shape specified to remove is not a child of the
     * NestingShape .
     *
     * @param child
     */
    public void remove(Shape child) {
        child.parent = null;
        this.shapes.remove(child);
    }

    /**
     * Returns the Shape at a specified position within a NestingShape . If the position
     * specified is less than zero or greater than the number of children stored in the
     * NestingShape less one this method throws an IndexOutOfBoundsException .
     *
     * @param index
     * @return
     * @throws IndexOutOfBoundsException
     */
    public Shape shapeAt(int index) throws IndexOutOfBoundsException {
        return this.shapes.get(index);
    }

    /**
     * Returns the number of children contained within a NestingShape object. Note this
     * method is not recursive - it simply returns the number of children at the top level
     * within the callee NestingShape object.
     *
     * @return
     */
    public int shapeCount() {
        return this.shapes.size();
    }

    /**
     * Returns the index of a specified child within a NestingShape object. If the Shape
     * specified is not actually a child of the NestingShape this method returns -1;
     * otherwise the value returned is in the range 0 .. shapeCount() - 1.
     *
     * @param child
     * @return
     */
    public int indexOf(Shape child) {
        if (!contains(child))
            return -1;
        else
            return this.shapes.indexOf(child);
    }

    /**
     * Returns true if the Shape argument is a child of the NestingShape object on which
     * this method is called, false otherwise.
     *
     * @param child
     * @return
     */
    public boolean contains(Shape child) {
        return this.shapes.contains(child);
    }


    /**
     * Method to be implemented by concrete subclasses to handle subclass
     * specific painting.
     * Paints a NestingShape object by drawing a rectangle around the edge of its
     * bounding box. The NestingShape object's children are then painted.
     *
     * @param painter the ictgradschool.industry.lab15.ex01.Painter object used for drawing.
     */
    @Override
    public void doPaint(Painter painter) {
        painter.drawRect(fX,fY,fWidth,fHeight);
        painter.translate(fX, fY);
        for (Shape shape : shapes) {
            shape.paint(painter);
        }
        painter.translate(-fX, -fY);
    }
}
